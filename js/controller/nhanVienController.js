function getValueForm() {
  var taikhoan = document.getElementById("tknv").value;
  var ten = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  var datepicker = document.getElementById("datepicker").value;
  var luongCB = document.getElementById("luongCB").value * 1;
  var chucVu = document.getElementById("chucvu").value * 1;
  var gioLam = document.getElementById("gioLam").value * 1;

  var newNV = new nhanVien(
    taikhoan,
    password,
    ten,
    email,
    datepicker,
    luongCB,
    chucVu,
    gioLam
  );
  return newNV;
}

function viewTable(arrNV) {
  var content = "";
  for (let i = 0; i < arrNV.length; i++) {
    content += `<tr>
      <td>${arrNV[i].taiKhoan}</td>
      <td>${arrNV[i].hoTen}</td>
      <td>${arrNV[i].email}</td>
      <td>${arrNV[i].ngayLam}</td>
      <td>${arrNV[i].chucVu()}</td>
      <td>${arrNV[i].tongLuong()}</td>
      <td>${arrNV[i].xepLoaiNV()}</td>
      <td>
        <button class="btn btn-info mx-1" onclick="edit('${
          arrNV[i].taiKhoan
        }')" >Sửa</button
        ><button class="btn btn-danger my-1" onclick="deleteNV('${
          arrNV[i].taiKhoan
        }')">Xoá</button>
      </td>
    </tr>`;
  }
  document.getElementById("tableDanhSach").innerHTML = content;
}

function edit(taiKhoan) {
  let i = findI(taiKhoan, arrNV);
  if (i !== -1) {
    document.getElementById("tknv").value = arrNV[i].taiKhoan;
    document.getElementById("name").value = arrNV[i].hoTen;
    document.getElementById("email").value = arrNV[i].email;
    document.getElementById("password").value = arrNV[i].matKhau;
    document.getElementById("datepicker").value = arrNV[i].ngayLam;
    document.getElementById("luongCB").value = arrNV[i].luongCoBan;
    document.getElementById("chucvu").value = arrNV[i].chucVuVal;
    document.getElementById("gioLam").value = arrNV[i].gioLam;
    document.querySelector("#btnThemNV").style.display = "none";
    document.querySelector("#btnCapNhat").style.display = "block";
    $("#myModal").modal("show");
  }
  console.log(taiKhoan);
}

document.querySelector("#btnThem").addEventListener("click", () => {
  document.querySelector("#myForm").reset();
  document.querySelector("#btnThemNV").style.display = "block";
  document.querySelector("#btnCapNhat").style.display = "none";
});

function deleteNV(taiKhoan) {
  let i = findI(taiKhoan, arrNV);
  if (i !== -1) {
    arrNV.splice(i, 1);
    viewTable(arrNV);
  }
}

function findI(taiKhoan, arr) {
  return arr.findIndex((e) => {
    return e.taiKhoan == taiKhoan;
  });
}
function saveInStorage(array, name) {
  var json = JSON.stringify(array);
  localStorage.setItem(name, json);
}
