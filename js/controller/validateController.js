var checkValidate = () => {
  let validate = new Validate();
  if (
    validate.kiemTraRegex(
      "tknv",
      "tbTKNV",
      /^[\w]{4,6}$/,
      "Tên đăng nhập gồm 4-6 ký số"
    ) &
    validate.kiemTraRegex(
      "name",
      "tbTen",
      /^([A-VXYỲỌÁẦẢẤỜỄÀẠẰỆẾÝỘẬỐŨỨĨÕÚỮỊỖÌỀỂẨỚẶÒÙỒỢÃỤỦÍỸẮẪỰỈỎỪỶỞÓÉỬỴẲẸÈẼỔẴẺỠƠÔƯĂÊÂĐa-vxyỳọáầảấờễàạằệếýộậốũứĩõúữịỗìềểẩớặòùồợãụủíỹắẫựỉỏừỷởóéửỵẳẹèẽổẵẻỡơôưăêâđ]+)((\s{1}[A-VXYỲỌÁẦẢẤỜỄÀẠẰỆẾÝỘẬỐŨỨĨÕÚỮỊỖÌỀỂẨỚẶÒÙỒỢÃỤỦÍỸẮẪỰỈỎỪỶỞÓÉỬỴẲẸÈẼỔẴẺỠƠÔƯĂÊÂĐa-vxyỳọáầảấờễàạằệếýộậốũứĩõúữịỗìềểẩớặòùồợãụủíỹắẫựỉỏừỷởóéửỵẳẹèẽổẵẻỡơôưăêâđ]+){1,})$/,
      "Tên không hợp lệ"
    ) &
    validate.kiemTraRegex(
      "email",
      "tbEmail",
      /^[\w]+([\.-]?[\w]+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$$/,
      "Email không hợp lệ"
    ) &
    validate.kiemTraRegex(
      "password",
      "tbMatKhau",
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,10}$/,
      "Mật khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)"
    ) &
    validate.kiemTraRegex(
      "datepicker",
      "tbNgay",
      /^\d{2}.\d{2}.\d{4}$/,
      "Ngày không được để trống"
    ) &
    validate.kiemTraChucVu("chucvu", "tbChucVu", "Chọn loại nhân viên hợp lệ") &
    validate.kiemTraLuong(
      "luongCB",
      "tbLuongCB",
      "Lương cơ bản từ 1.000.000 - 20.000.000"
    ) &
    validate.kiemTraGioLam("gioLam", "tbGiolam", "Giờ làm từ 80 - 200 giờ")
  ) {
    console.log("đúng");
    return true;
  } else {
    console.log("sai");
    return false;
  }
};
