var arrNV = [];
var LocalStorageArr = "LocalStorage_NV";
var arrStorage = localStorage.getItem(LocalStorageArr);
if (arrStorage) {
  arrNVTemp = JSON.parse(arrStorage);
  arrNVTemp.forEach((e, i) => {
    var newNV = new nhanVien(
      e.taiKhoan,
      e.matKhau,
      e.hoTen,
      e.email,
      e.ngayLam,
      e.luongCoBan,
      e.chucVuVal,
      e.gioLam
    );
    arrNV.push(newNV);
  });
  console.log(arrNV);
  viewTable(arrNV);
}
document.getElementById("btnThemNV").addEventListener("click", () => {
  if (checkValidate()) {
    var newNV = getValueForm();
    console.log(newNV.taiKhoan);
    var index = arrNV.findIndex((item) => {
      console.log("taikhoan", item.taiKhoan);

      return item.taiKhoan == newNV.taiKhoan;
    });
    console.log(index);
    if (index === -1) {
      arrNV.push(getValueForm());
      saveInStorage(arrNV, LocalStorageArr);
      document.getElementById("tbTKNV").innerHTML = ``;
    } else {
      document.getElementById("tbTKNV").innerHTML = `Tài khoản đã tồn tại`;
    }
  }
  console.log(arrNV);

  viewTable(arrNV);
});

document.getElementById("btnCapNhat").addEventListener("click", () => {
  if (checkValidate()) {
    const updateNV = getValueForm();
    let i = findI(updateNV.taiKhoan, arrNV);
    if (i === -1) {
      document.getElementById("tbTKNV").innerHTML = `Tài khoản không tồn tại`;
    } else {
      document.getElementById("tbTKNV").innerHTML = ``;
      arrNV[i] = updateNV;
      saveInStorage(arrNV, LocalStorageArr);
      viewTable(arrNV);
    }
  }
});

const elSearch = document.getElementById("searchName");
elSearch.addEventListener("keyup", () => {
  arr = [];
  let value = elSearch.value.trim();
  arrNV.forEach((e, i) => {
    if (e.xepLoaiNV().includes(value)) {
      arr.push(e);
    }
  });
  viewTable(arr);
});
