var nhanVien = function (
  _taikhoan,
  _matkhau,
  _hoTen,
  _email,
  _ngayLam,
  _luongCoBan,
  _chucVu,
  _gioLam
) {
  this.taiKhoan = _taikhoan;
  this.matKhau = _matkhau;
  this.hoTen = _hoTen;
  this.email = _email;
  this.ngayLam = _ngayLam;
  this.luongCoBan = _luongCoBan;
  this.chucVuVal = _chucVu;
  this.chucVu = () => {
    switch (this.chucVuVal) {
      case 1:
        return "Giám đốc";
      case 2:
        return "Trưởng phòng";
      case 3:
        return "Nhân viên";
    }
  };
  this.gioLam = _gioLam;
  this.tongLuong = () => {
    switch (_chucVu) {
      case 1:
        return this.luongCoBan * 3;
      case 2:
        return this.luongCoBan * 2;
      case 3:
        return this.luongCoBan * 1;
    }
  };
  this.xepLoaiNV = () => {
    if (this.gioLam >= 192) {
      return "Xuất sắc";
    } else if (this.gioLam >= 176) {
      return "Giỏi";
    } else if (this.gioLam >= 160) {
      return "Khá";
    } else {
      return "Trung bình";
    }
  };
};
