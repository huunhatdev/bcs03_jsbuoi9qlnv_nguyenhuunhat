const Validate = function () {
  this.kiemTraRegex = (idTarget, idErr, pattern, errNoti) => {
    let _value = document.getElementById(idTarget).value.trim();
    let _pattern = new RegExp(pattern);
    console.log(errNoti);
    document.getElementById(idErr).style.display = "block";
    if (_pattern.test(_value)) {
      document.getElementById(idErr).innerHTML = ``;
      return true;
    }
    document.getElementById(idErr).innerHTML = errNoti;
    return false;
  };

  this.kiemTraLuong = (idTarget, idErr, errNoti) => {
    document.getElementById(idErr).style.display = "block";
    let _value = document.getElementById(idTarget).value * 1;
    if (_value >= 1000000 && _value <= 20000000) {
      document.getElementById(idErr).innerHTML = ``;
      return true;
    }
    document.getElementById(idErr).innerHTML = errNoti;
    return false;
  };

  this.kiemTraGioLam = (idTarget, idErr, errNoti) => {
    document.getElementById(idErr).style.display = "block";
    let _value = document.getElementById(idTarget).value * 1;
    console.log(errNoti);
    if (_value >= 80 && _value <= 200) {
      document.getElementById(idErr).innerHTML = ``;
      return true;
    }
    document.getElementById(idErr).innerHTML = errNoti;
    return false;
  };

  this.kiemTraChucVu = (idTarget, idErr, errNoti) => {
    document.getElementById(idErr).style.display = "block";
    let _value = document.getElementById(idTarget).value * 1;
    if (_value >= 1 && _value <= 3) {
      document.getElementById(idErr).innerHTML = ``;
      return true;
    }
    document.getElementById(idErr).innerHTML = errNoti;
    return false;
  };
};
